const gliderText = `.O
..O
OOO`
function arrConverse(str){
    let arr = [], converseArr = []
    let str2 = str.split("\n")
    for(let i in str2){
        arr.push(str2[i].split(""))
    }
    
    for(let i in arr){
        converseArr[i]=[]
        for(let j in arr[i]){
            if(arr[i][j] == "."){
                converseArr[i].push(0)
            }else if(arr[i][j] == "O"){
                converseArr[i].push(1)
            }
        }
    }
    return converseArr
}
for(let i = 0 ; i < 10; i++){
    let star = ""
    for(let a = 0; a < i; a++){
        star = star + " "
    }
    for(let b = 10; b > i; b--){
        star = star + "*"
    }
}
let currentBoard = "";
for(let i = 0 ; i < 12; i++){
    let star = ""
    for(let a = 0; a < i; a++){
        star = star + " "
    }
    for(let b = 11; b > i; b--){
        star = star + "*"
    }
    for(let b = 11; b > i; b--){
        star = star + "*"
    }
    currentBoard += star
    currentBoard += "*"
    currentBoard += "\n"
}
function arrConverseAgain(str){
    let arr = [], converseArr = []
    let str2 = str.split("\n")
    for(let i in str2){
        arr.push(str2[i].split(""))
    }
    
    for(let i = 0; i < arr.length; i++){
        converseArr[i]=[]
        for(let j = 0; j < arr[i].length; j++){
            if(arr[i][j] == "*"){
                converseArr[i].push(1)
            }else{
                converseArr[i].push(0)
            }
        }
    }
    return converseArr
}
let nextBoard = arrConverseAgain(currentBoard)
nextBoard.pop()
function hexToRGB(h) {
    let r = 0, g = 0, b = 0;
    // 3 digits //
    if (h.length == 4) {
        r = "0x" + h[1] + h[1];
        g = "0x" + h[2] + h[2];
        b = "0x" + h[3] + h[3];
        // 6 digits //
    } else if (h.length == 7) {
        r = "0x" + h[1] + h[2];
        g = "0x" + h[3] + h[4];
        b = "0x" + h[5] + h[6];
    }
    r = parseInt(r)
    g = parseInt(g)
    b = parseInt(b)
    return [r, g, b];
}
function fillInHexShadowColor(color){
    let [r, g, b] = hexToRGB(color)
    for(let i = 0; i < columns; i++){
        for(let j = 0; j < rows; j++){
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            if(currentBoard[i][j] == 1 && nextBoard[i][j] == 0){
                fill(`rgba(${r},${g},${b},0.75)`)
            }else if(currentBoard[i][j] == 1 && nextBoard[i][j] == 1){
                fill(`rgba(${r},${g},${b},1)`)
            }else if(currentBoard[i][j] == 0 && nextBoard[i][j] == 1){
                fill(`rgba(${r},${g},${b},0.5)`)
            }
            else{
                fill(backgroundColorPicker.color())
            }
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
            stroke(strokeColor);
        }
    }
}
const boxColor  =  "#cad2d4";
function checkPatternText(str){
    for(let i = 0; i < str.length; i++){
        if(str[i] != "." || str[i] != "O" || str[i] != "" || str[i] != "\n" || str[i] != " "){
            alert("You type incorrect pattern, please type again")
            return false
        }
    }
    return true
}
let i = 0
let j = 0
let iOld = i
let iNew = i + 2
let jOld = j
for(let x = 0; x < 2; x++){
    for(let y = 0; y < 2; y++){
        i++
        if(i == iNew){
            i = iOld
            j++
        }
    }
}
iOldPosition = iOld
jOldPosition = jOld
function movingUpTrackSquareColor(){
    if(currentBoard[iOldPosition][jOldPosition] == 0){
        for(let x = 0; x < 2 ; x++){
            fill(backgroundColorPicker.color())
            rect(iOldPosition * unitLength, jOldPosition * unitLength, unitLength, unitLength)
            stroke(strokeColor)
            iOldPosition++
        }
    }
}
function movingUpCurrentSquareColor(){
    for(let x = 0; x < 2 ; x++){
        fill(colorPicker.color())
        rect(i * unitLength, j * unitLength, unitLength, unitLength)
        stroke(strokeColor)
        i++
    }
    i = i - 2
    iOldPosition = i
    jOldPosition = j
}
function movingRightTrackSquareColor(){
    if(currentBoard[iOldPosition][jOldPosition] == 0){
        for(let x = 0; x < 2 ; x++){
            fill(backgroundColorPicker.color())
            rect(iOldPosition * unitLength, jOldPosition * unitLength, unitLength, unitLength)
            stroke(strokeColor)
            jOldPosition++
        }
    }
}
function movingRightCurrentSquareColor(){
    for(let x = 0; x < 2 ; x++){
        fill(colorPicker.color())
        rect(i * unitLength, j * unitLength, unitLength, unitLength)
        stroke(strokeColor)
        j++
    }
    iOldPosition = i
    j = j - 2
    jOldPosition = j
}
function movingDownTrackSquareColor(){
    if(currentBoard[iOldPosition][jOldPosition] == 0){
        for(let x = 0; x < 2 ; x++){
            fill(backgroundColorPicker.color())
            rect(iOldPosition * unitLength, jOldPosition * unitLength, unitLength, unitLength)
            stroke(strokeColor)
            iOldPosition++
        }
    }
}
function movingDownCurrentSquareColor(){
    for(let x = 0; x < 2 ; x++){
        fill(colorPicker.color())
        rect(i * unitLength, j * unitLength, unitLength, unitLength)
        stroke(strokeColor)
        i++
    }
    i = i - 2
    iOldPosition = i
    jOldPosition = j
}
let str = "12"
let str2 = "12".split("\n")
for(let i = 0; i < str2.length; i++){
    for(let j = 0; j < str2[i].length; j++){
        console.log(isNaN(parseInt(str2[i][j])))
        console.log(isNaN(parseInt("O")))
        console.log(str2[i][j])
        console.log(parseInt(str2[i][j]))
    }
}
console.log("\n")
for(let i = 0; i < str2.length; i++){
    for(let j = 0; j < str2[i].length; j++){
        console.log(str2[i][j])
        console.log(isNaN(parseInt(str2[i][j])))
        console.log(isNaN(parseInt("O")))
        console.log(parseInt(str2[i][j]))
    }
}
backgroundColorPicker.color() = {mode: "rgb", maxes: {hsb: [360, 100, 100, 1], hsl: [360, 100, 100, 1], rgb: [255, 255, 255, 255]}, _array: [0.8941176470588236, 0.8941176470588236, 0.8941176470588236, 1], levels: [228, 228, 228, 255]}
backgroundColorPicker.color().mode = "rgb"
backgroundColorPicker.color().maxes.hsb[0] = 360
backgroundColorPicker.color().maxes.hsb[1] = 100
backgroundColorPicker.color().maxes.hsb[2] = 100
backgroundColorPicker.color().maxes.hsb[3] = 1
backgroundColorPicker.color().maxes.hsl[0] = 360
backgroundColorPicker.color().maxes.hsl[1] = 100
backgroundColorPicker.color().maxes.hsl[2] = 100
backgroundColorPicker.color().maxes.hsl[3] = 1
backgroundColorPicker.color().maxes.rgb[0] = 255
backgroundColorPicker.color().maxes.rgb[1] = 255
backgroundColorPicker.color().maxes.rgb[2] = 255
backgroundColorPicker.color().maxes.rgb[3] = 255
backgroundColorPicker.color()._array[0] = 0.8941176470588236
backgroundColorPicker.color()._array[1] = 0.8941176470588236
backgroundColorPicker.color()._array[2] = 0.8941176470588236
backgroundColorPicker.color()._array[3] = 1
backgroundColorPicker.color().levels[0] = 228
backgroundColorPicker.color().levels[1] = 228
backgroundColorPicker.color().levels[2] = 228
backgroundColorPicker.color().levels[3] = 255

colorPicker.color() = {mode: "rgb", maxes: {hsb: [360, 100, 100, 1], hsl: [360, 100, 100, 1], rgb: [255, 255, 255, 255]}, _array: [0.20784313725490197,  0.20784313725490197,  0.20784313725490197, 1], levels: [53, 53, 53, 255]}
colorPicker.color().mode = "rgb"
colorPicker.color().maxes.hsb[0] = 360
colorPicker.color().maxes.hsb[1] = 100
colorPicker.color().maxes.hsb[2] = 100
colorPicker.color().maxes.hsb[3] = 1
colorPicker.color().maxes.hsl[0] = 360
colorPicker.color().maxes.hsl[1] = 100
colorPicker.color().maxes.hsl[2] = 100
colorPicker.color().maxes.hsl[3] = 1
colorPicker.color().maxes.rgb[0] = 255
colorPicker.color().maxes.rgb[1] = 255
colorPicker.color().maxes.rgb[2] = 255
colorPicker.color().maxes.rgb[3] = 255
colorPicker.color()._array[0] = 0.20784313725490197
colorPicker.color()._array[1] = 0.20784313725490197
colorPicker.color()._array[2] = 0.20784313725490197
colorPicker.color()._array[3] = 1
colorPicker.color().levels[0] = 53
colorPicker.color().levels[1] = 53
colorPicker.color().levels[2] = 53
colorPicker.color().levels[3] = 255

console.log(backgroundColorPicker.color())
console.log(colorPicker.color())