const gosperGliderGunButton = document.querySelector(".gosper-glider-gun")
const resetBackgroundColor = document.querySelector('#backgroundColor')
const overpopulationValue = document.querySelector(".overpopulation")
const lightweightTrain = document.querySelector(".lightweight-train")
const switchEngineButton = document.querySelector(".switch-engine")
const darkbackground = document.querySelector(".background-color")
const unitLengthValue = document.querySelector(".unitlengthValue")
const reproductionValue = document.querySelector(".reproduction")
const blinkerShipButton = document.querySelector(".blinker-ship")
const movingRightButton = document.querySelector(".movingright")
const removeColorButton = document.querySelector("#minusButton")
const submitPatternNow = document.querySelector(".submitButton")
const customUnitLength = document.querySelector("#customRange2")
const framerateValue = document.querySelector(".framerateValue")
const herschelButton = document.querySelector(".herschel-shape")
const fillInPattern2 = document.querySelector(".patternTextRLE")
const resetStableColor = document.querySelector('#stableColor')
const blackButtonList = document.querySelector(".listchanging")
const originalColor = document.querySelector(".original-color")
const movingLeftButton = document.querySelector(".movingleft")
const movingDownButton = document.querySelector(".movingdown")
const randomGameButton = document.querySelector(".random-tab")
const checkDarkMode = document.querySelector("#exampleCheck1")
const generationValue = document.querySelector(".generation")
const lonelinessValue = document.querySelector(".loneliness")
const resetGameButton = document.querySelector("#reset-game")
const addColorButton = document.querySelector("#plusButton")
const fillInPattern = document.querySelector(".patternText")
const resetLiveColor = document.querySelector('#editColor')
const startButton = document.querySelector("#start-button")
const singleColor = document.querySelector(".single-color")
const shadowColor = document.querySelector(".shadow-color")
const randomColor = document.querySelector(".random-color")
const movingUpButton = document.querySelector(".movingup")
const blackButton = document.querySelector(".three-line")
const gliderButton = document.querySelector(".glider")
const greySquare2 = document.querySelector("#square2")
const greySquare = document.querySelector("#square1")
const rate = document.querySelector("#customRange1")
const getCanvas = document.querySelector("#canvas")
const backgroundColor = "#e4e4e4"
const newLifeColor = "#353535"
const stableColor = "#eea473"
let gosperGliderGunText = `........................O
......................O.O
............OO......OO............OO
...........O...O....OO............OO
OO........O.....O...OO
OO........O...O.OO....O.O
..........O.....O.......O
...........O...O
............OO`
let blinkerShipText = `..........OOOO
..........O...O
..........O
.OO........O..O
OO.OO
.OOOO...O
..OO...O.OO........O....OOO
......O...O........O....O.O
..OO...O.OO........O....OOO
.OOOO...O
OO.OO
.OO........O..O
..........O
..........O...O
..........OOOO`
let schickEngineText = `.O..O
O
O...O
OOOO.........OO
......OOO.....OO
......OO.OO......OOO
......OOO.....OO
OOOO.........OO
O...O
O
.O..O`
let switchEngineText = `.O.O
O
.O..O
...OOO`
let herschelText = `O
OOO
O.O
..O`
let gliderText = `.O
..O
OOO`
let columns //To be determined by window width
let rows //To be determined by window height 
let strokeColor = "#a7a6a6"
let currentBackgroundColor
let currentLiveColor
let backgroundColorPicker
let collapseNumber = 0
let gridlineNumber = 0
let patternNumber = 0
let darkKeyNumber = 1
let stableColorPicker
let iOldPosition = 0
let jOldPosition = 0
let squareNumber = 0
let chooseColor = 0
let unitLength = 15
let darkNumber = 1
let currentBoard
let intervalID1
let intervalID2
let intervalID3
let intervalID4
let colorPicker
let destroy = 0
let living = 0
let nextBoard
let count = 0
let turn = 0
let canvas
let i = 0
let j = 0
movingRightButton.addEventListener("mousedown", movingRightPress)
gosperGliderGunButton.addEventListener("click",gosperGliderGun)
movingDownButton.addEventListener("mousedown", movingDownPress)
movingLeftButton.addEventListener("mousedown", movingLeftPress)
unitLengthValue.innerHTML = parseInt(customUnitLength.value)
customUnitLength.addEventListener("input",unitLengthChange)
movingUpButton.addEventListener("mousedown", movingUpPress)
switchEngineButton.addEventListener("click", switchEngine)
randomGameButton.addEventListener("click", initialRandom)
blinkerShipButton.addEventListener("click", blinkerShip)
removeColorButton.addEventListener("click", removeColor)
checkDarkMode.addEventListener("click", triggerDarkMode)
lightweightTrain.addEventListener("click",schickEngine)
addColorButton.addEventListener("click", fillAddColor) 
originalColor.addEventListener("click",colororiginal)
greySquare2.addEventListener("click",gridlineDelete)
rate.addEventListener("input", framerateChangeValue)
resetGameButton.addEventListener("click",resetGame)
submitPatternNow.addEventListener("click",pattern)
herschelButton.addEventListener("click",herschel)
singleColor.addEventListener("click",colorsingle)
shadowColor.addEventListener("click",colorshadow)
randomColor.addEventListener("click",colorrandom)
startButton.addEventListener("click",startGame)
framerateValue.innerHTML = parseInt(rate.value)
greySquare.addEventListener("click",addSquare)
gliderButton.addEventListener("click",glider)
function changingColor(){
    background(backgroundColorPicker.color())
    fillInColor(colorPicker.color())
}
function stableChangingColor(){
    background(backgroundColorPicker.color())
    fillInColor(stableColorPicker.color())
}
function fillInColor(color){
    for(let i = 0; i < columns; i++){
        for(let j = 0; j < rows; j++){
            if(currentBoard[i][j] == 1){
                fill(color)
            }else{
                fill(currentBackgroundColor)
            }
            rect(i * unitLength, j * unitLength, unitLength, unitLength)
            stroke(strokeColor)
        }
    }
}
function fillInShadowColor(color){
    for(let i = 0; i < columns; i++){
        for(let j = 0; j < rows; j++){
            if(currentBoard[i][j] == 1 && nextBoard[i][j] == 0){
                fill(`rgba(${color.levels[0]},${color.levels[1]},${color.levels[2]},0.75)`)
            }else if(currentBoard[i][j] == 1 && nextBoard[i][j] == 1){
                fill(`rgba(${color.levels[0]},${color.levels[1]},${color.levels[2]},1)`)
            }else if(currentBoard[i][j] == 0 && nextBoard[i][j] == 1){
                fill(`rgba(${color.levels[0]},${color.levels[1]},${color.levels[2]},0.5)`)
            }
            else{
                fill(currentBackgroundColor)
            }
            rect(i * unitLength, j * unitLength, unitLength, unitLength)
            stroke(strokeColor)
        }
    }
}
function hexToRGB(h){
    let r = 0
    let g = 0
    let b = 0
    if(h.length == 4){ //3 digits
        r = "0x" + h[1] + h[1]
        g = "0x" + h[2] + h[2]
        b = "0x" + h[3] + h[3]
    }else if(h.length == 7){ //6 digits
        r = "0x" + h[1] + h[2];
        g = "0x" + h[3] + h[4];
        b = "0x" + h[5] + h[6];
    }
    r = parseInt(r)
    g = parseInt(g)
    b = parseInt(b)
    return [r, g, b];
}
function fillInHexShadowColor(color){
    let [r, g, b] = hexToRGB(color)
    for(let i = 0; i < columns; i++){
        for(let j = 0; j < rows; j++){
            if(currentBoard[i][j] == 1 && nextBoard[i][j] == 0){
                fill(`rgba(${r},${g},${b},0.75)`)
            }else if(currentBoard[i][j] == 1 && nextBoard[i][j] == 1){
                fill(`rgba(${r},${g},${b},1)`)
            }else if(currentBoard[i][j] == 0 && nextBoard[i][j] == 1){
                fill(`rgba(${r},${g},${b},0.5)`)
            }
            else{
                fill(currentBackgroundColor)
            }
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
            stroke(strokeColor);
        }
    }
}
function checkEqual(board1,board2){
    let isLoop = true
    for(let i = 0; i < board1.length; i++){
        if(isLoop){
            for(let j = 0; j < board1[i].length; j++){
                if(board1[i][j] != board2[i][j]){
                    isLoop = false
                    break
                }
            }
        }
    }
    return isLoop
}
function arrConverse(str){
    let arr = []
    let conversedArr = []
    let str2 = str.split("\n")
    for(let i = 0; i < str2.length; i++){
        arr.push(str2[i].split(""))
    }
    for(let i = 0; i < arr.length; i++){
        conversedArr[i]=[]
        for(let j in arr[i]){
            if(arr[i][j] == "."){
                conversedArr[i].push(0)
            }else if(arr[i][j] == "O"){
                conversedArr[i].push(1)
            }
        }
    }
    return conversedArr
}
function arrConverseRLE(str){
    let arr = []
    let conversedArr = []
    let turn = ""
    let cell = 0
    let str2 = str.split("$")
    for(let i = 0; i < str2.length; i++){
        arr.push(str2[i].split(""))
    }
    for(let i = 0; i < arr.length; i++){
        conversedArr[i] = []
        for(let j = 0; j < arr[i].length; j++){
            switch(arr[i][j]){
                case "b":
                    cell = 0
                    if(turn != ""){
                        turn = parseInt(turn)
                        for(let y = 0; y < turn; y++){
                            conversedArr[i].push(cell)
                        }
                    }else{
                        conversedArr[i].push(cell)
                    }
                    turn = ""
                    break
                case "o":
                    cell = 1
                    if(turn != ""){
                        turn = parseInt(turn)
                        for(let y = 0; y < turn; y++){
                            conversedArr[i].push(cell)
                        }
                    }else{
                        conversedArr[i].push(cell)
                    }
                    turn = ""
                    break
                default:
                    turn += arr[i][j]
                    break
            }
        }
    }
    return conversedArr
}
function resetGame(){
    resetLiveColor.value = "#353535"
    resetBackgroundColor.value = "#e4e4e4"
    resetStableColor.value = "#eea473"
    startButton.innerHTML = '<div class="start"><i class="fas fa-play-circle"></i></div>'
    removeColorButton.classList.remove("movingactive")
    addColorButton.classList.remove("movingactive")
    greySquare.classList.remove("square-active")
    getCanvas.classList.remove("canvas-active")
    generationValue.innerHTML = 0
    unitLengthValue.innerHTML = 15
    overpopulationValue.value = 3
    framerateValue.innerHTML = 40
    reproductionValue.value = 3
    customUnitLength.value = 15
    lonelinessValue.value = 2
    fillInPattern2.value = ""
    fillInPattern.value = ""
    patternNumber = 0
    squareNumber = 0
    chooseColor = 0
    rate.value = 40
    unitLength = 15
    destroy = 0
    living = 0
    turn = 0
    i = 0
    j = 0
    columns = floor((windowWidth - 10) / unitLength)
    rows = floor((windowHeight - 100) / unitLength)
    width = columns * unitLength
    height = rows * unitLength
    canvas = createCanvas(width, height)
    canvas.parent(document.querySelector('#canvas'))
    currentBoard = []
    nextBoard = []
    for(let i = 0; i < columns; i++){
        currentBoard[i] = []
        nextBoard[i] = []
    }
    init()
    fillInColor(backgroundColor)
    frameRate(40)
    startGame()
    noLoop()
    count = 0
}
function startGame(){
    if(turn == 0){
        startButton.innerHTML = '<div class="start"><i class="fas fa-pause-circle"></i></div>'
        loop()
        turn = 1
    }else if(turn == 1){
        startButton.innerHTML = '<div class="start"><i class="fas fa-play-circle"></i></div>'
        noLoop()
        turn = 0
    }
    fillInPattern2.value = ""
    fillInPattern.value = ""
}
function colorshadow(){
    chooseColor = 0
}
function colororiginal(){
    chooseColor = 1
}
function colorsingle(){
    chooseColor = 2
}
function colorrandom(){
    chooseColor = 3
}
function fillAddColor(){
    switch(patternNumber){
        case 1:
            drawGosperGliderGun()
            break
        case 2:
            drawGlider()
            break
        case 3:
            drawSchickEngine()
            break
        case 4:
            drawHerschel()
            break
        case 5:
            drawSwitchEngine()
            break
        case 6:
            drawPattern()
            break
        case 7:
            drawPatternRLE()
            break
        case 8:
            drawBlinkerShip()
            break
        default:
            fill(currentLiveColor)
            rect(i * unitLength, j * unitLength, unitLength, unitLength)
            stroke(strokeColor)
            currentBoard[i][j] = 1
            break
    }
}
function fillAddColorKeyboard(){
    if(living == 0){
        living = 1
        addColorButton.classList.add("movingactive")
    }else{
        living = 0
        addColorButton.classList.remove("movingactive")
    }
}
function removeColor(){
    if(destroy == 0){
        if(currentBoard[i][j] == 1){
            fill(currentBackgroundColor)
            rect(i * unitLength, j * unitLength, unitLength, unitLength)
            stroke(strokeColor)
            currentBoard[i][j] = 0
        }
        destroy = 1
        removeColorButton.classList.add("movingactive")
        getCanvas.classList.add("canvas-active")
    }else{
        destroy = 0
        removeColorButton.classList.remove("movingactive")
        getCanvas.classList.remove("canvas-active")
    }
}
function initialRandom(){
    for(let i = 0; i < columns; i++){
        for(let j = 0; j < rows; j++){
            if(Math.random() > 0.8){ 
                currentBoard[i][j] = 1
            }else{
                currentBoard[i][j] = 0
            }
        }
    }
    fillInColor(currentLiveColor)
}
function gosperGliderGun(){
    patternNumber = 1
}
function glider(){
    patternNumber = 2
}
function schickEngine(){
    patternNumber = 3
}
function herschel(){
    patternNumber = 4
}
function switchEngine(){
    patternNumber = 5
}
function blinkerShip(){
    patternNumber = 8
}
function framerateChangeValue(event){
    let speed = parseInt(event.currentTarget.value)
    frameRate(speed)
    framerateValue.innerHTML = speed
}
function updateCanvas(){
    width = columns * unitLength
    height = rows * unitLength
    canvas = createCanvas(width, height)
    canvas.parent(document.querySelector('#canvas'))
    let c = color(random(255),random(100,200),random(100),random(200,255))
    background(currentBackgroundColor)
    if(checkEqual(currentBoard,nextBoard)){
        fillInColor(stableColorPicker.color())
        startGame()
    }else{
        if(chooseColor == 0){
            if(darkNumber == 0){
                fillInShadowColor(currentLiveColor)
            }else{
                fillInHexShadowColor(currentLiveColor)
            }
        }else if(chooseColor == 1){
            fillInColor(currentLiveColor)
        }else if(chooseColor == 2){
            fillInColor(c)
        }else if(chooseColor == 3){
            for(let i = 0; i < columns; i++){
                for(let j = 0; j < rows; j++){
                    if(currentBoard[i][j] == 1){
                        c = color(random(255),random(100,200),random(100),random(200,255))
                        fill(c)
                    }else{
                        fill(currentBackgroundColor)
                    }
                    rect(i * unitLength, j * unitLength, unitLength, unitLength)
                    stroke(strokeColor)
                }
            }
        }
    }
}
function unitLengthChange(event){
    let newUnitLength = parseInt(event.currentTarget.value)
    unitLength = newUnitLength
    unitLengthValue.innerHTML = newUnitLength
    updateCanvas()
}
function movingLeftPressSingle(){
    if(destroy == 1 && currentBoard[i][j] == 1){
        fillCurrentColor(currentBackgroundColor)
        currentBoard[i][j] = 0
    }else if(living == 1 && squareNumber == 0){
        fillCurrentColor(currentLiveColor)
        currentBoard[i][j] = 1
        i = (i - 1 + columns) % columns  
    }else if(squareNumber == 1){
        if(living == 1){
            movingCurrentSquareColor()
            movingCurrentSquareColorAddCurrentboard()
            i = (i - 1 + columns) % columns  
        }else{
            movingTrackSquareColor() //delete color in square when destory == 1
            i = (i - 1 + columns) % columns  
            movingCurrentSquareColor()
        }
    }else{
        fillTrackColor(currentBackgroundColor) 
        i = (i - 1 + columns) % columns  
        fillCurrentColor(currentLiveColor)
    }
}
function movingUpPressSingle(){
    if(destroy == 1 && currentBoard[i][j] == 1){
        fillCurrentColor(currentBackgroundColor)
        currentBoard[i][j] = 0
    }else if(living == 1 && squareNumber == 0){
        fillCurrentColor(currentLiveColor)
        currentBoard[i][j] = 1
        j = (j - 1 + rows) % rows 
    }else if(squareNumber == 1){
        if(living == 1){
            movingCurrentSquareColor()
            movingCurrentSquareColorAddCurrentboard()
            j = (j - 1 + rows) % rows 
        }else{
            movingTrackSquareColor()
            j = (j - 1 + rows) % rows  
            movingCurrentSquareColor()
        }
    }else{
        fillTrackColor(currentBackgroundColor)
        j = (j - 1 + rows) % rows 
        fillCurrentColor(currentLiveColor)
    }
}
function movingRightPressSingle(){
    if(destroy == 1 && currentBoard[i][j] == 1){
        fillCurrentColor(currentBackgroundColor)
        currentBoard[i][j] = 0
    }else if(living == 1 && squareNumber == 0){
        fillCurrentColor(currentLiveColor)
        currentBoard[i][j] = 1
        i = (i + 1 + columns) % columns 
    }else if(squareNumber == 1){
        if(living == 1){
            movingCurrentSquareColor()
            movingCurrentSquareColorAddCurrentboard()
            i = (i + 1 + columns) % columns 
        }else{
            movingTrackSquareColor()
            i = (i + 1 + columns) % columns 
            movingCurrentSquareColor()
        }
    }else{
        fillTrackColor(currentBackgroundColor)
        i = (i + 1 + columns) % columns 
        fillCurrentColor(currentLiveColor)
    }        
}
function movingDownPressSingle(){
    if(destroy == 1 && currentBoard[i][j] == 1){
        fillCurrentColor(currentBackgroundColor)
        currentBoard[i][j] = 0
    }else if(living == 1 && squareNumber == 0){
        fillCurrentColor(currentLiveColor)
        currentBoard[i][j] = 1
        j = (j + 1 + rows) % rows 
    }else if(squareNumber == 1){
        if(living == 1){
            movingCurrentSquareColor()
            movingCurrentSquareColorAddCurrentboard()
            j = (j + 1 + rows) % rows 
        }else{
            movingTrackSquareColor()
            j = (j + 1 + rows) % rows 
            movingCurrentSquareColor()
        }
    }else{
        fillTrackColor(currentBackgroundColor)
        j = (j + 1 + rows) % rows 
        fillCurrentColor(currentLiveColor)
    }
}
function movingLeftPress(){
    intervalID1 = setInterval(movingLeftPressSingle, 65)
}
function movingUpPress(){
    intervalID2 = setInterval(movingUpPressSingle, 65)
}
function movingRightPress(){
    intervalID3 = setInterval(movingRightPressSingle, 65)
}
function movingDownPress(){
    intervalID4 = setInterval(movingDownPressSingle, 65)
}
function pattern(){
    if(checkString(fillInPattern.value) && (checkStringRLE(fillInPattern2.value) == false || fillInPattern2.value == "")){
        let newPatternArr = arrConverse(fillInPattern.value)
        for(let i = 0; i < newPatternArr.length && i < rows; i++){
            for(let j = 0; j < newPatternArr[i].length && j < columns; j++){
                currentBoard[j][i] = newPatternArr[i][j]
            }
        }
        fillInColor(currentLiveColor)
        patternNumber = 6
    }else if((checkString(fillInPattern.value) == false || fillInPattern.value == "") && checkStringRLE(fillInPattern2.value)){
        patternNumber = 7
    }else{
        alert("you type wrong format")
    }
}
function drawPattern(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    let a = Math.floor(mouseX / unitLength)
    let b = Math.floor(mouseY / unitLength)
    let newPatternArr = arrConverse(fillInPattern.value)
    for(let i = 0; i < newPatternArr.length && i < rows; i++){ //for the rows may be smaller than newPatternArr.length
        for(let j = 0; j < newPatternArr[i].length && j < columns; j++){
            currentBoard[(a + j + columns) % columns][(b + i + rows) % rows] = newPatternArr[i][j]
        }
    }
    fillInColor(currentLiveColor)
}
function drawPatternRLE(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    let a = Math.floor(mouseX / unitLength)
    let b = Math.floor(mouseY / unitLength)
    let newPatternArr = arrConverseRLE(fillInPattern2.value)
    for(let i = 0; i < newPatternArr.length && i < rows; i++){
        for(let j = 0; j < newPatternArr[i].length && j < columns; j++){
            currentBoard[(a + j + columns) % columns][(b + i + rows) % rows] = newPatternArr[i][j]
        }
    }
    fillInColor(currentLiveColor)
}
function drawGosperGliderGun(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    let a = Math.floor(mouseX / unitLength)
    let b = Math.floor(mouseY / unitLength)
    let goseperGliderGunArray = arrConverse(gosperGliderGunText)
    for(let i = 0; i < goseperGliderGunArray.length && i < rows; i++){
        for(let j = 0; j < goseperGliderGunArray[i].length && j < columns; j++){
            currentBoard[(a + j + columns) % columns][(b + i + rows) % rows] = goseperGliderGunArray[i][j]
        }
    }
    fillInColor(currentLiveColor)
}
function drawGlider(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    let a = Math.floor(mouseX / unitLength)
    let b = Math.floor(mouseY / unitLength)
    let gliderArray = arrConverse(gliderText)
    for(let i = 0; i < gliderArray.length && i < rows; i++){
        for(let j = 0; j < gliderArray[i].length && j < columns; j++){
            currentBoard[(a + j + columns) % columns][(b + i + rows) % rows] = gliderArray[i][j]
        }
    }
    fillInColor(currentLiveColor)
}
function drawSchickEngine(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    let a = Math.floor(mouseX / unitLength)
    let b = Math.floor(mouseY / unitLength)
    let schickEngineArray = arrConverse(schickEngineText)
    for(let i = 0; i < schickEngineArray.length && i < rows; i++){
        for(let j = 0; j < schickEngineArray[i].length && j < columns; j++){
            currentBoard[(a + j + columns) % columns][(b + i + rows) % rows] = schickEngineArray[i][j]
        }
    }
    fillInColor(currentLiveColor)
}
function drawHerschel(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    let a = Math.floor(mouseX / unitLength)
    let b = Math.floor(mouseY / unitLength)
    let herschelArray = arrConverse(herschelText)
    for(let i = 0; i < herschelArray.length && i < rows; i++){
        for(let j = 0; j < herschelArray[i].length && j < columns; j++){
            currentBoard[(a + j + columns) % columns][(b + i + rows) % rows] = herschelArray[i][j]
        }
    }
    fillInColor(currentLiveColor)
}
function drawSwitchEngine(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    let a = Math.floor(mouseX / unitLength)
    let b = Math.floor(mouseY / unitLength)
    let switchEngineArray = arrConverse(switchEngineText)
    for(let i = 0; i < switchEngineArray.length && i < rows; i++){
        for(let j = 0; j < switchEngineArray[i].length && j < columns; j++){
            currentBoard[(a + j + columns) % columns][(b + i + rows) % rows] = switchEngineArray[i][j]
        }
    }
    fillInColor(currentLiveColor)
}
function drawBlinkerShip(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    let a = Math.floor(mouseX / unitLength)
    let b = Math.floor(mouseY / unitLength)
    let blinkerShipArray = arrConverse(blinkerShipText)
    for(let i = 0; i < blinkerShipArray.length && i < rows; i++){
        for(let j = 0; j < blinkerShipArray[i].length && j < columns; j++){
            currentBoard[(a + j + columns) % columns][(b + i + rows) % rows] = blinkerShipArray[i][j]
        }
    }
    fillInColor(currentLiveColor)
}
function addSquare(){
    if(squareNumber == 0){
        squareNumber = 1
        greySquare.classList.add("square-active")
        if(living == 0){
            fillCurrentColor(currentBackgroundColor)
        }
        movingCurrentSquareColor()
        if(living == 1){
            movingCurrentSquareColorAddCurrentboard()
        }
        iOldPosition = i
        jOldPosition = j
    }else{
        squareNumber = 0
        greySquare.classList.remove("square-active")
        if(living == 0){
            movingTrackSquareColor()
            fillCurrentColor(currentLiveColor)
        }
    }
}
function gridlineDelete(){
    if(gridlineNumber == 0){
        strokeColor = currentBackgroundColor
        greySquare2.classList.add("square-active")
        fillInColor(currentLiveColor)
        gridlineNumber = 1
    }else if(gridlineNumber == 1){
        strokeColor = "#a7a6a6"
        greySquare2.classList.remove("square-active")
        fillInColor(currentLiveColor)
        gridlineNumber = 0
    }
}
function checkString(str){
    let str2 = str.split("\n")
    let isLoop = true
    for(let i = 0; i < str2.length; i++){
        if(isLoop){
            for(let j = 0; j < str2[i].length; j++){
                if(!(str2[i][j] == "." || str2[i][j] == "O" || str2[i][j] == "" || str2[i][j] == " ")){ //if(str2[i][j] != "." || str2[i][j] != "O" || str2[i][j] != "" || str2[i][j] != " ") cannot return expected result, because of de Morgan's law
                   isLoop = false
                   break
                }
            }
        }
    }
    return isLoop
}
function checkStringRLE(str){
    let str2 = str.split("$")
    let isLoop = true
    for(let i = 0; i < str2.length; i++){
        for(let j = 0; j < str2[i].length; j++){
            if(str2[i][j] != "b" && str2[i][j] != "o" && str2[i][j] != "!" && str2[i][j] != "" && str2[i][j] != " " && isNaN(parseInt(str2[i][j]))){
                isLoop = false
                break
            }
        }
    }
    return isLoop
}
function fillTrackColor(color){
    if(currentBoard[iOldPosition][jOldPosition] == 0){
        fill(color)
        rect(iOldPosition * unitLength, jOldPosition * unitLength, unitLength, unitLength)
        stroke(strokeColor)
    }
}
function fillCurrentColor(color){
    fill(color)
    rect(i * unitLength, j * unitLength, unitLength, unitLength)
    stroke(strokeColor)
    iOldPosition = i
    jOldPosition = j
}
function movingTrackSquareColor(){
    let iOld = iOldPosition
    let iNew = iOldPosition + 2
    if(currentBoard[iOldPosition][jOldPosition] == 0){
        for(let x = 0; x < 2; x++){
            for(let y = 0; y < 2; y++){
                fill(currentBackgroundColor)
                rect(iOldPosition * unitLength, jOldPosition * unitLength, unitLength, unitLength)
                stroke(strokeColor)
                iOldPosition++
                if(iOldPosition == iNew){
                    iOldPosition = iOld
                    jOldPosition++
                }
            }
        }
    }
}
function movingCurrentSquareColor(){
    let iOld = i
    let iNew = i + 2
    let jOld = j
    for(let x = 0; x < 2; x++){
        for(let y = 0; y < 2; y++){
            fill(currentLiveColor)
            rect(i * unitLength, j * unitLength, unitLength, unitLength)
            stroke(strokeColor)
            i++
            if(i == iNew){
                i = iOld
                j++
            }
        }
    }
    iOldPosition = iOld
    jOldPosition = jOld
    i = iOld
    j = jOld
}
function movingCurrentSquareColorAddCurrentboard(){
    let iOld = i
    let iNew = i + 2
    let jOld = j
    for(let x = 0; x < 2; x++){
        for(let y = 0; y < 2; y++){
            currentBoard[(i + columns) % columns][(j + rows) % rows] = 1
            i++
            if(i == iNew){
                i = iOld
                j++
            }
        }
    }
    i = iOld
    j = jOld
}
function triggerDarkMode(){
    if(darkNumber == 0){
        darkNumber = 1
        currentBackgroundColor = "#8b8b8b"
        currentLiveColor = "#e6e6e6"
        fillInColor(currentLiveColor)
        darkbackground.classList.add("darkbackground-color")
    }else{
        darkNumber = 0
        currentBackgroundColor = backgroundColorPicker.color()
        currentLiveColor = colorPicker.color()
        fillInColor(currentLiveColor)
        darkbackground.classList.remove("darkbackground-color")
    }
}
function triggerDarkModeKeyboard(){
    if(darkKeyNumber == 0){
        checkDarkMode.checked = true
        darkKeyNumber = 1
    }else{
        checkDarkMode.checked = false
        darkKeyNumber = 0
    }
}
function mouseDragged(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    const x = Math.floor(mouseX / unitLength)
    const y = Math.floor(mouseY / unitLength)
    if(destroy == 0){
        if(squareNumber == 1){
            i = x
            j = y
            movingCurrentSquareColor()
            movingCurrentSquareColorAddCurrentboard()
        }else{
            currentBoard[(x + columns) % columns][(y + rows) % rows] = 1
            fill(currentLiveColor)
            stroke(strokeColor)
            rect(x * unitLength, y * unitLength, unitLength, unitLength)
        }      
    }else if(destroy == 1){
        currentBoard[(x + columns) % columns][(y + rows) % rows] = 
        fill(currentBackgroundColor)
        stroke(strokeColor)
        rect(x * unitLength, y * unitLength, unitLength, unitLength)
    }
}
function mousePressed(){
    switch(patternNumber){
        case 1:
            drawGosperGliderGun()
            break
        case 2:
            drawGlider()
            break
        case 3:
            drawSchickEngine()
            break
        case 4:
            drawHerschel()
            break
        case 5:
            drawSwitchEngine()
            break
        case 6:
            drawPattern()
            break
        case 7:
            drawPatternRLE()
            break
        case 8:
            drawBlinkerShip()
            break
    }
}
function mouseMoved(){
    if(mouseX > unitLength * columns || mouseY > unitLength * rows || mouseX < 0 || mouseY < 0){
        return
    }
    i = Math.floor(mouseX / unitLength)
    j = Math.floor(mouseY / unitLength)
}
function keyPressed(){
    switch(keyCode){
        case 65:
            movingLeftPress()
            movingLeftButton.classList.add("movingactive")        
            break
        case 68:
            movingRightPress()
            movingRightButton.classList.add("movingactive")   
            break
        case 74:
            initialRandom()
            randomGameButton.classList.add("active")
            break
        case 75:
            startGame()
            startButton.classList.add("active")
            break
        case 76:
            resetGame()
            resetGameButton.classList.add("active")
            break
        case 73:
            addSquare()
            break
        case 79:
            gridlineDelete()
            break
        case 80:
            triggerDarkMode()
            triggerDarkModeKeyboard()
            break
        case 83:
            movingDownPress()    
            movingDownButton.classList.add("movingactive")  
            break
        case 87:
            movingUpPress()
            movingUpButton.classList.add("movingactive")         
            break
        case 90:
            $('.collapse').collapse("toggle")
            break
        case 187:
            fillAddColor()
            fillAddColorKeyboard()
            break
        case 189:
            removeColor()
            break
    }
}
function keyReleased(){
    switch(keyCode){
        case 65:
            clearInterval(intervalID1)
            movingLeftButton.classList.remove("movingactive")        
            break
        case 68:
            clearInterval(intervalID3)
            movingRightButton.classList.remove("movingactive")   
            break
        case 74:
            randomGameButton.classList.remove("active")
            break
        case 75:
            startButton.classList.remove("active")
            break
        case 76:
            resetGameButton.classList.remove("active")
            break
        case 83: 
            clearInterval(intervalID4)
            movingDownButton.classList.remove("movingactive")  
            break
        case 87:
            clearInterval(intervalID2)
            movingUpButton.classList.remove("movingactive")         
            break
    }
}
function windowResized(){
    unitLength = min(floor((windowWidth + 100) / columns), floor((windowHeight + 100) / rows))
    unitLengthValue.innerHTML = unitLength
    customUnitLength.value = unitLength
    updateCanvas()
}

function setup(){
    startGame()
    noLoop()
    colorPicker = createColorPicker(newLifeColor)
    colorPicker.parent(document.querySelector('#editColor'))
    backgroundColorPicker = createColorPicker(backgroundColor)
    backgroundColorPicker.parent(document.querySelector('#backgroundColor'))
    backgroundColorPicker.input(changingColor)
    stableColorPicker = createColorPicker(stableColor)
    stableColorPicker.parent(document.querySelector('#stableColor'))
    stableColorPicker.input(stableChangingColor)
    columns = floor((windowWidth - 10) / unitLength)
    rows = floor((windowHeight - 100) / unitLength)
    width = columns * unitLength
    height = rows * unitLength
    canvas = createCanvas(width, height)
    canvas.parent(document.querySelector('#canvas'))
    currentBoard = []
    nextBoard = []
    for(let i = 0; i < columns; i++){
        currentBoard[i] = []
        nextBoard[i] = []
    }
    currentBackgroundColor = backgroundColorPicker.color()
    currentLiveColor = colorPicker.color()
    init() //Set the initial values of the currentBoard and nextBoard
    frameRate(40)
}
function init(){
    for(let i = 0; i < columns; i++){
        for(let j = 0; j < rows; j++){
            currentBoard[i][j] = 0
            nextBoard[i][j] = 0
        }
    }
}
function draw(){
    if(darkNumber == 0){
        currentBackgroundColor = backgroundColorPicker.color()
        currentLiveColor = colorPicker.color()
    }else{
        currentBackgroundColor = "#8b8b8b"
        currentLiveColor = "#e6e6e6"
    }
    let c = color(random(255),random(100,200),random(100),random(200,255))
    background(currentBackgroundColor)
    generate()
    if(checkEqual(currentBoard,nextBoard)){
        fillInColor(stableColorPicker.color())
        startGame()
    }else{
        if(chooseColor == 0){
            if(darkNumber == 0){
                fillInShadowColor(currentLiveColor)
            }else{
                fillInHexShadowColor(currentLiveColor)
            }
        }else if(chooseColor == 1){
            fillInColor(currentLiveColor)
        }else if(chooseColor == 2){
            fillInColor(c)
        }else if(chooseColor == 3){
            for(let i = 0; i < columns; i++){
                for(let j = 0; j < rows; j++){
                    if(currentBoard[i][j] == 1){
                        c = color(random(255),random(100,200),random(100),random(200,255))
                        fill(c)
                    }else{
                        fill(currentBackgroundColor)
                    }
                    rect(i * unitLength, j * unitLength, unitLength, unitLength)
                    stroke(strokeColor)
                }
            }
        }
    }
    generationValue.innerHTML = count
    count++
}
function generate(){
    for(let x = 0; x < columns; x++){
        for(let y = 0; y < rows; y++){
            let neighbors = 0
            for(let i of [-1, 0, 1]){
                for(let j of [-1, 0, 1]){
                    if(i == 0 && j == 0){
                        neighbors += 0
                    }else{
                        neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows]
                    }
                }
            }
            if(currentBoard[x][y] == 1 && neighbors < lonelinessValue.value){
                nextBoard[x][y] = 0
            }else if(currentBoard[x][y] == 1 && neighbors > overpopulationValue.value){
                nextBoard[x][y] = 0
            }else if(currentBoard[x][y] == 0 && neighbors == reproductionValue.value){
                nextBoard[x][y] = 1
            }else{
                nextBoard[x][y] = currentBoard[x][y]
            }
        }
    }
    [currentBoard, nextBoard] = [nextBoard, currentBoard]
}